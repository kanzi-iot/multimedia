﻿Files:
PropertyTypeLibrary/ButtonPlay.IsPaused
PropertyTypeLibrary/CubemapColor
PropertyTypeLibrary/MusicItem.ThisSongIsSelected
PropertyTypeLibrary/MusicPlayer.CurrentMusicProgress
PropertyTypeLibrary/MusicPlayer.CurrentSongDuration
PropertyTypeLibrary/MusicPlayer.CurrentSongImagePath
PropertyTypeLibrary/MusicPlayer.MusicPlayer.DataContext
PropertyTypeLibrary/MusicPlayer.NextSongIndex
PropertyTypeLibrary/MusicPlayer.PrevSongIndex
PropertyTypeLibrary/MusicPlayer.SelectedSongIndex
PropertyTypeLibrary/MusicPlayer.TrackIndex
PropertyTypeLibrary/offset
PropertyTypeLibrary/ProgressBarSongDuration
PropertyTypeLibrary/smoothing
PropertyTypeLibrary/Texture2
PropertyTypeLibrary/TextureCube
PropertyTypeLibrary/TAG_PROPERTY_TYPE_Transparent
Animation Clips/Animation Clip
Screens/Screen/MusicPlayer/Text Block 2D
Screens/Screen/<ResourceDictionaryInNode>
Screens/Screen/MusicPlayer
Screens/Screen
Material Types/VertexPhong
Material Types/VertexPhongCube
Material Types/VertexPhongMorph
Material Types/VertexPhongSkinned
Material Types/VertexPhongSkinnedMorph
Material Types/VertexPhongTextured
Material Types/VertexPhongTexturedCube
Material Types/VertexPhongTexturedMorph
Material Types/VertexPhongTexturedSkinned
Material Types/VertexPhongTexturedSkinnedMorph
Material Types/ColorTexture
Material Types/DefaultBlit
Material Types/FontDefault
Material Types/MaskTexture
Material Types/SweepTextures
Material Types/Textured
Materials/VertexPhongMaterial
Materials/VertexPhongCubeMaterial
Materials/VertexPhongMorphMaterial
Materials/VertexPhongSkinnedMaterial
Materials/VertexPhongSkinnedMorphMaterial
Materials/VertexPhongTexturedMaterial
Materials/VertexPhongTexturedCubeMaterial
Materials/VertexPhongTexturedMorphMaterial
Materials/VertexPhongTexturedSkinnedMaterial
Materials/VertexPhongTexturedSkinnedMorphMaterial
Materials/ColorTextureMaterial
Materials/DefaultBlitMaterial
Materials/FontDefaultMaterial
Materials/MaskTextureMaterial
Materials/SweepTexturesMaterial
Materials/TexturedMaterial
Brushes/DefaultBackground
Brushes/Color Brush
Brushes/Color Brush_1
Prefabs/MusicPlayer/MusicPlayer/Node 2D Volume/NodeVolumeActual/Image
Prefabs/MusicPlayer/MusicPlayer/Node 2D Volume/ImageFull
Prefabs/MusicPlayer/MusicPlayer/Node 2D Volume/NodeVolumeActual
Prefabs/MusicPlayer/MusicPlayer/Node 2D Volume/Image
Prefabs/MusicPlayer/MusicPlayer/Buttons/Button 2D Remix/Image
Prefabs/MusicPlayer/MusicPlayer/Buttons/Button 2D Prev/Image
Prefabs/MusicPlayer/MusicPlayer/Buttons/Button 2D Play/Image
Prefabs/MusicPlayer/MusicPlayer/Buttons/Button 2D Next/Image
Prefabs/MusicPlayer/MusicPlayer/Buttons/Button 2D Air Flow/Image
Prefabs/MusicPlayer/MusicPlayer/Buttons/Button 2D Remix
Prefabs/MusicPlayer/MusicPlayer/Buttons/Button 2D Prev
Prefabs/MusicPlayer/MusicPlayer/Buttons/Button 2D Play
Prefabs/MusicPlayer/MusicPlayer/Buttons/Button 2D Next
Prefabs/MusicPlayer/MusicPlayer/Buttons/Button 2D Air Flow
Prefabs/MusicPlayer/MusicPlayer/ProgressBar/CurrentProgres/ImageCurrentProgres
Prefabs/MusicPlayer/MusicPlayer/ProgressBar/ImageAllProgress
Prefabs/MusicPlayer/MusicPlayer/ProgressBar/CurrentProgres
Prefabs/MusicPlayer/MusicPlayer/ProgressBar/Text Block 2D
Prefabs/MusicPlayer/MusicPlayer/<ResourceDictionaryInNode>
Prefabs/MusicPlayer/MusicPlayer/Grid List Box 2D
Prefabs/MusicPlayer/MusicPlayer/Node 2D Volume
Prefabs/MusicPlayer/MusicPlayer/SelectedSongImage
Prefabs/MusicPlayer/MusicPlayer/Buttons
Prefabs/MusicPlayer/MusicPlayer/ProgressBar
Prefabs/MusicPlayer/MusicPlayer
Prefabs/MusicItem/MusicItem/ImageCover
Prefabs/MusicItem/MusicItem/ArtistName
Prefabs/MusicItem/MusicItem/SongName
Prefabs/MusicItem/MusicItem/SongDuration
Prefabs/MusicItem/MusicItem
Prefabs/List Box Item Container 2D/List Box Item Container 2D/Dock Layout 2D/Separator
Prefabs/List Box Item Container 2D/List Box Item Container 2D/Dock Layout 2D/Content Layout 2D
Prefabs/List Box Item Container 2D/List Box Item Container 2D/Dock Layout 2D
Prefabs/List Box Item Container 2D/List Box Item Container 2D
Prefabs/MusicPlayer
Prefabs/MusicItem
Prefabs/List Box Item Container 2D
Data Sources/Data source
Page Transitions/Default Transitions
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posX_1x1.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negX_1x1.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posY_1x1.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negY_1x1.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds posZ_1x1.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_128x128.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_64x64.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_32x32.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_16x16.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_8x8.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_4x4.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_2x2.png
Resource Files/Images/cubemaps/mipmaps/DefaultCubeMap.dds negZ_1x1.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds posX.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds negX.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds posY.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds negY.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds posZ.png
Resource Files/Images/cubemaps/DefaultCubeMap.dds negZ.png
Resource Files/Images/mipmaps/DefaultTextureImage_512x512.png
Resource Files/Images/mipmaps/DefaultTextureImage_256x256.png
Resource Files/Images/mipmaps/DefaultTextureImage_128x128.png
Resource Files/Images/mipmaps/DefaultTextureImage_64x64.png
Resource Files/Images/mipmaps/DefaultTextureImage_32x32.png
Resource Files/Images/mipmaps/DefaultTextureImage_16x16.png
Resource Files/Images/mipmaps/DefaultTextureImage_8x8.png
Resource Files/Images/mipmaps/DefaultTextureImage_4x4.png
Resource Files/Images/mipmaps/DefaultTextureImage_2x2.png
Resource Files/Images/mipmaps/DefaultTextureImage_1x1.png
Resource Files/Images/DefaultBackground.png
Resource Files/Images/DefaultTextureImage.png
Resource Files/Images/Music20.png
Resource Files/Images/Music19.png
Resource Files/Images/Music18.png
Resource Files/Images/Music17.png
Resource Files/Images/Music16.png
Resource Files/Images/Music15.png
Resource Files/Images/Music14.png
Resource Files/Images/Music13.png
Resource Files/Images/Music12.png
Resource Files/Images/Music11.png
Resource Files/Images/Music10.png
Resource Files/Images/Music9.png
Resource Files/Images/Music8.png
Resource Files/Images/Music7.png
Resource Files/Images/Music6.png
Resource Files/Images/Music5.png
Resource Files/Images/Music4.png
Resource Files/Images/Music3.png
Resource Files/Images/Music2.png
Resource Files/Images/Music1.png
Resource Files/Images/Air flow inside.png
Resource Files/Images/Next.png
Resource Files/Images/Play.png
Resource Files/Images/Last.png
Resource Files/Images/Remix.png
Resource Files/Images/Pause.png
Resource Files/Images/Actual time.png
Resource Files/Images/All time.png
Resource Files/Images/Full scale.png
Resource Files/Images/Volume.png
Resource Files/Images/icon volume.png
Resource Files/Fonts/NotoSans-Bold.ttf
Textures/DefaultBackground
Textures/Default Texture
Textures/Default Cube Map Texture
Textures/Music20
Textures/Music19
Textures/Music18
Textures/Music17
Textures/Music16
Textures/Music15
Textures/Music14
Textures/Music13
Textures/Music12
Textures/Music11
Textures/Music10
Textures/Music9
Textures/Music8
Textures/Music7
Textures/Music6
Textures/Music5
Textures/Music4
Textures/Music3
Textures/Music2
Textures/Music1
Textures/Air flow inside
Textures/Next
Textures/Play
Textures/Last
Textures/Remix
Textures/Pause
Textures/Actual time
Textures/All time
Textures/Full scale
Textures/Volume
Textures/icon volume
Styles/Text Block 2D Style
State Managers/MusicItem State Manager
State Managers/Button 2D Play State Manager
Font Families/Noto Sans
/musicplayer
