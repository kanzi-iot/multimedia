cmake_minimum_required(VERSION 3.5.1)
project(multimedia)

include(${CMAKE_CURRENT_LIST_DIR}/cmake/kanzi-locate.cmake)

if(MSVC)
    option(BUILD_SHARED_LIBS "Selects whether to build and link to shared or static libraries" ON)
endif()

if(NOT KANZI_ENGINE_BUILD)
    find_kanzi()
    find_package(Kanzi REQUIRED CONFIG CMAKE_FIND_ROOT_PATH_BOTH)
endif()

include(kanzi-common)

add_executable(multimedia src/multimedia.cpp)

if(ANDROID)
    target_link_libraries(multimedia -Wl,--whole-archive Kanzi::kzappfw -Wl,--no-whole-archive)
else()
    target_link_libraries(multimedia Kanzi::kzappfw)
endif()

target_link_libraries(multimedia Kanzi::kzui Kanzi::kzcoreui)

# KANZI_LINK_FREETYPE and KANZI_LINK_ITYPE options determine which font backends are explicitly linked to your application. 
# By default including Kanzi config sets the following:
#  - KANZI_LINK_FREETYPE=ON in static build.
#  - Both OFF in dynamic build.
#
# In case you are working with dynamic libraries, Kanzi engine can load available font engine backend at runtime.
# Use FontEngine application configuration to choose which font engine backend Kanzi loads: 
# FontEngine=freetype|itype|none
target_link_font_engine_backends(multimedia)

# [CodeBehind libs start]. Do not remove this identifier.
set(multimedia_codebehind_lib_dir "${CMAKE_CURRENT_SOURCE_DIR}/../Tool_project/CodeBehind/multimedia")
if(EXISTS "${multimedia_codebehind_lib_dir}")
    add_subdirectory("${multimedia_codebehind_lib_dir}" "${CMAKE_CURRENT_BINARY_DIR}/CodeBehind/multimedia")
    include_directories(${multimedia_codebehind_lib_dir}/include)
    target_link_libraries(multimedia multimedia_CodeBehind)
endif()
# [MusicPlayer CodeBehind lib start]. Do not remove or change this identifier.
set(MusicPlayer_codebehind_lib_dir "${CMAKE_CURRENT_SOURCE_DIR}/Subprojects/MusicPlayer/CodeBehind/MusicPlayer")
if(EXISTS "${MusicPlayer_codebehind_lib_dir}")
    add_subdirectory("${MusicPlayer_codebehind_lib_dir}" "${CMAKE_CURRENT_BINARY_DIR}/CodeBehind/MusicPlayer")
    include_directories(${MusicPlayer_codebehind_lib_dir}/include)
    target_link_libraries(multimedia MusicPlayer_CodeBehind)
endif()
# [MusicPlayer CodeBehind lib end]. Do not remove or change this identifier.
# [LockScreen CodeBehind lib start]. Do not remove or change this identifier.
set(LockScreen_codebehind_lib_dir "${CMAKE_CURRENT_SOURCE_DIR}/Subprojects/LockScreen/CodeBehind/LockScreen")
if(EXISTS "${LockScreen_codebehind_lib_dir}")
    add_subdirectory("${LockScreen_codebehind_lib_dir}" "${CMAKE_CURRENT_BINARY_DIR}/CodeBehind/LockScreen")
    include_directories(${LockScreen_codebehind_lib_dir}/include)
    target_link_libraries(multimedia LockScreen_CodeBehind)
endif()
# [LockScreen CodeBehind lib end]. Do not remove or change this identifier.
# [BorderAndMenuButtons CodeBehind lib start]. Do not remove or change this identifier.
set(BorderAndMenuButtons_codebehind_lib_dir "${CMAKE_CURRENT_SOURCE_DIR}/Subprojects/BorderAndMenuButtons/CodeBehind/BorderAndMenuButtons")
if(EXISTS "${BorderAndMenuButtons_codebehind_lib_dir}")
    add_subdirectory("${BorderAndMenuButtons_codebehind_lib_dir}" "${CMAKE_CURRENT_BINARY_DIR}/CodeBehind/BorderAndMenuButtons")
    include_directories(${BorderAndMenuButtons_codebehind_lib_dir}/include)
    target_link_libraries(multimedia BorderAndMenuButtons_CodeBehind)
endif()
# [BorderAndMenuButtons CodeBehind lib end]. Do not remove or change this identifier.
# [MainMultimediaPage CodeBehind lib start]. Do not remove or change this identifier.
set(MainMultimediaPage_codebehind_lib_dir "${CMAKE_CURRENT_SOURCE_DIR}/Subprojects/MainMultimediaPage/CodeBehind/MainMultimediaPage")
if(EXISTS "${MainMultimediaPage_codebehind_lib_dir}")
    add_subdirectory("${MainMultimediaPage_codebehind_lib_dir}" "${CMAKE_CURRENT_BINARY_DIR}/CodeBehind/MainMultimediaPage")
    include_directories(${MainMultimediaPage_codebehind_lib_dir}/include)
    target_link_libraries(multimedia MainMultimediaPage_CodeBehind)
endif()
# [MainMultimediaPage CodeBehind lib end]. Do not remove or change this identifier.
# [CodeBehind libs end]. Do not remove this identifier.

set_target_properties(multimedia PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}/bin")
set_target_properties(multimedia PROPERTIES VS_DEBUGGER_ENVIRONMENT "${KANZI_VS_DEBUGGER_ENVIRONMENT}")

install_kanzi_libs_to_output_directory()
install_kzbs_to_output_directory(${CMAKE_SOURCE_DIR}/bin)
install_target_to_output_directory(multimedia)
